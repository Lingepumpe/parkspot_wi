GmapCoordinatesPicker.configure do |config|
  config.lat_column = :xCoor
  config.lng_column = :yCoor
  config.default_coordinates = [47.263573, 11.345261]
  #config.map_handler = 'gMapObj' #javascript map object
  config.zoom_level = 15
  #config.map_container_class = 'gmap_coordinate_picker_container'
  config.map_width = '600px'
  config.map_height = '400px'
end
