class ChangeParkidFormat < ActiveRecord::Migration
  def change
    change_column :ParkingSpot, :ParkID, :string
    change_column :RepeatedOffer, :ParkID, :string
  end
end
