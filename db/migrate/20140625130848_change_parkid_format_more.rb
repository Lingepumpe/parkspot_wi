class ChangeParkidFormatMore < ActiveRecord::Migration
  def change
   change_column :Occupy, :ParkID, :string 
   change_column :Offer, :ParkID, :string
   change_column :Reserve, :ParkID, :string
  end
end
