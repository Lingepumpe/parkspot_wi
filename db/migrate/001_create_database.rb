# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

class CreateDatabase < ActiveRecord::Migration
  def self.up

ActiveRecord::Schema.define(version: 0) do

  create_table "Occupy", id: false, force: true do |t|
    t.string    "ParkID", limit: 11, default: "", null: false
    t.integer   "UserID",            default: 0,  null: false
    t.timestamp "Start",                          null: false
    t.timestamp "End"
  end

  add_index "Occupy", ["UserID"], name: "UserID", using: :btree

  create_table "Offer", id: false, force: true do |t|
    t.string    "ParkID", limit: 11, null: false
    t.timestamp "End",               null: false
  end

  create_table "ParkingSpot", primary_key: "ParkID", force: true do |t|
    t.integer "UserID", null: false
    t.float   "xCoor"
    t.float   "yCoor"
    t.float   "Price",  null: false
  end

  add_index "ParkingSpot", ["UserID"], name: "UserID", using: :btree

  create_table "RepeatedOffer", primary_key: "ParkID", force: true do |t|
    t.timestamp "Start",            null: false
    t.timestamp "End",              null: false
    t.string    "Day",   limit: 11
  end

  create_table "Reserve", id: false, force: true do |t|
    t.string    "ParkID", limit: 11, default: "", null: false
    t.integer   "UserID",            default: 0,  null: false
    t.timestamp "End",                            null: false
  end

  add_index "Reserve", ["UserID"], name: "UserID", using: :btree

  create_table "User", primary_key: "UserID", force: true do |t|
    t.string "UserName", limit: 30, null: false
    t.float  "Credits",             null: false
    t.string "PW_Hash",  limit: 90, null: false
  end

  add_index "User", ["UserName"], name: "UserName", unique: true, using: :btree

end

end

def self.down
  #drop all the tables if you need to support migration back to
  #version 0
end
end

