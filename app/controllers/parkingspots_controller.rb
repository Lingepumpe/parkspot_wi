class ParkingspotsController < ApplicationController
  def index
    if current_user
      @parkingspots = Parkingspot.where(UserID: current_user.UserID)
    else
      @parkingspots = Parkingspot.all
    end
  end
  def show
    @parkingspot = Parkingspot.find(params[:id])
    @qr = RQRCode::QRCode.new(params[:id], :size => 4, :level => :h )
    @state, @state_by = getState params[:id]
  end
  def new
  end
  def create
    @parkingspot = Parkingspot.new(parkingspot_params)
    @parkingspot.Price = 10
    @parkingspot.xCoor = params[:xCoor]
    @parkingspot.yCoor = params[:yCoor]
    if current_user
      @parkingspot.UserID = current_user.UserID
    end
    @parkingspot.save
    redirect_to @parkingspot
  end
  def destroy
    @parkingspot = Parkingspot.find(params[:id])
    @parkingspot.destroy
    redirect_to parkingspots_path
  end


private
  def parkingspot_params
    params.require(:parkingspot).permit(:UserID, :ParkID, :xCoor, :yCoor)
  end
  def getState(parkID)
    #@state can be "Not Offered" "Offered" "Reserved" "Occupied"
    currentTime = Time.now + 3600*2 #utc + 2 O_o
    occ = Occupy.where("ParkID = ? AND (End is NULL OR End > ?)", parkID, currentTime).take
    if occ
      return "Occupied for " + getTime(occ.End, currentTime), occ.UserID
    end
    res = Reserve.where("ParkID = ? AND (End is NULL OR End > ?)", parkID, currentTime).take
    if res
      return "Reserved for " + getTime(res.End, currentTime), res.UserID
    end
    off = Offer.where("ParkID = ? AND (End is NULL OR End > ?)", parkID, currentTime).take
    if off
      return "Offered for " + getTime(off.End, currentTime), 0
    end
    return "Not Offered", 0
  end
  helper_method :getState
  def getTime myTime, currentTime
    if myTime == nil
      return "ever"
    end
    return humanize(myTime - currentTime)
  end
  def humanize secs
    secs = secs / 60
    [[60, :minutes], [24, :hours], [1000, :days]].inject([]){ |s, (count, name)|
    if secs > 0 
      secs, n = secs.divmod(count)
      s.unshift "#{n.to_i} #{name}"
    end
    s
    }.join(' ')
  end
end
