class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by_UserName(params[:UserName])
    firstHash = Digest::SHA256.hexdigest(params[:password]) 
    if user && Digest::SHA256.hexdigest(firstHash) == user.PW_Hash
      session[:user_id] = user.id
      redirect_to parkingspots_path, notice: "Logged in!"
    else
      flash.now.alert = "User name or password is invalid"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Logged out"
  end
end
