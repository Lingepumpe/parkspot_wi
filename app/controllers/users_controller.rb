class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if params[:user][:password] != params[:user][:password_confirmation]
      redirect_to root_url
    end
    @user.Credits = 150
    firstHashed = Digest::SHA256.hexdigest(params[:user][:password])
    @user.PW_Hash = Digest::SHA256.hexdigest(firstHashed) 
    if @user.save
      session[:user_id] = @user.id
      redirect_to parkingspots_path, notice: "Thank you for signing up!"
    else
      redirect_to root_url
    end
  end

private
  def user_params
    params.require(:user).permit(:UserName)
  end
end
