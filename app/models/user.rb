class User < ActiveRecord::Base
  self.table_name = "User"
  validates_uniqueness_of :UserName
end
